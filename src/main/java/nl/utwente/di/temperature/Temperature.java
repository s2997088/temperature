package nl.utwente.di.temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets degrees Celsius and outputs degrees Fahrenheit
 */

public class Temperature extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Translator translator;
	
    public void init() throws ServletException {
    	translator = new Translator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius to Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Degrees Celsius: " +
                   request.getParameter("degrees celsius") + "\n" +
                "  <P>Degrees Fahrenheit: " +
                   translator.translateCelsius(request.getParameter("degrees celsius")) +
                "</BODY></HTML>");
  }
}
