package nl.utwente.di.temperature;

public class Translator {

    double toDouble(String toTest) {
        try {
            return(Double.parseDouble(toTest));
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }
    double translateCelsius(String celsius) {
        return (toDouble(celsius) * 9/5) + 32;
    }
}
