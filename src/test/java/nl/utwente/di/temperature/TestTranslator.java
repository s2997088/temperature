package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** * Tests the Translator */
public class TestTranslator {

    @Test
    public void testBook1() throws Exception {
        Translator translator = new Translator();
        double price = translator.translateCelsius("20");
        Assertions.assertEquals(68.0, price , 0.0, "Degrees in celsius");
    }
}
